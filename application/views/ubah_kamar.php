<!DOCTYPE html>

<html>

<head>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>



<div class="container">

   <h3>Ubah Data Kamar</h3>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th></th>

        <th></th>

      </tr>

    </thead>

    <tbody>

      <form action="<?php echo base_url().'index.php/welcome/update';?>" method="post">

        <?php

        foreach ($data as $key => $value) { ?>

          <tr>

            <td width="100">Nomor kamar</td>

            <td width="300">

              <input type="hidden" class="form-control" value="<?php echo $value->id_kamar;?>" name="id_kamar" placeholder="Nomor kamar">

              <input type="text" class="form-control" value="<?php echo $value->nomor_kamar;?>" name="nomor_kamar" placeholder=" Nomor kamar">

            </td>

          </tr>



          <tr>

            <td width="100">Tipe kamar</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->id_kamar_tipe;?>" name="id_kamar_tipe" placeholder="Tipe kamar">
              


            </td>

          </tr>



          <tr>

            <td width="100">Max dewasa</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->max_dewasa;?>" name="max_dewasa" placeholder="Max dewasa">

            </td>

          </tr>
          <tr>

            <td width="100">Max anak</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->max_anak;?>" name="max_anak" placeholder="Max anak">

            </td>

          </tr>

          <tr>

            <td width="100">Status</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->status_kamar;?>" name="status_kamar" placeholder="Status kamar">

            </td>

          </tr>

        <?php

        }

        ?>

          <tr>

            <td colspan="2">

              <a href="<?php echo base_url().'index.php/welcome/';?>" class="btn btn-info">Batal</a>

              <button type="submit" class="btn btn-primary">Simpan</button>

            </td>

          </tr>

    </form>

    </tbody>

  </table>

  </div>

</div>


</body>

</html>